# docker_wget
A simple utility which allows downloading blobs / manifests from a Docker registry,
especially from the official registry (registry-1.docker.io).

This tools handles the following authentication mechanisms:
- [x] No auth
- [x] Bearer auth
- [ ] Basic auth