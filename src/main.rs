use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;

use futures_util::StreamExt;
use regex::Regex;
use reqwest::header;

enum HeadRequestResult {
    NotFound,
    UnauthorizedRealm {
        realm: String,
        service: String,
        scope: String,
    },
    UnauthorizedBasic,
    Ok,
    Failed(u16),
}

#[allow(dead_code)]
enum AuthOption {
    Bearer(String),
    Basic(String, Option<String>),
}

#[allow(dead_code)]
#[derive(serde::Deserialize)]
struct AuthResult {
    token: String,
    access_token: String,
    expires_in: u16,
    issued_at: Option<String>,
}

async fn head_request(url: &str) -> Result<HeadRequestResult, Box<dyn std::error::Error>> {
    let response = reqwest::Client::new().head(url).send().await?;

    let status = response.status().as_u16();

    Ok(
        match (status, response.headers().get(header::WWW_AUTHENTICATE)) {
            (200, _) => HeadRequestResult::Ok,
            (404, _) => HeadRequestResult::NotFound,
            (401, Some(h)) => {
                let h: &header::HeaderValue = h;
                let header = String::from_utf8(h.as_bytes().to_vec())?;

                let regex_basic = Regex::new(r###"^Basic"###).unwrap();
                let regex_bearer = Regex::new(r###"^Bearer realm="(?P<realm>[^"]+)",service="(?P<service>[^"]+)",scope="(?P<scope>[^"]+)""###).unwrap();

                if regex_basic.is_match(&header) {
                    HeadRequestResult::UnauthorizedBasic
                } else if let Some(cap) = regex_bearer.captures(&header) {
                    HeadRequestResult::UnauthorizedRealm {
                        realm: cap["realm"].to_string(),
                        service: cap["service"].to_string(),
                        scope: cap["scope"].to_string(),
                    }
                } else {
                    HeadRequestResult::Failed(401)
                }
            }
            (c, _) => HeadRequestResult::Failed(c),
        },
    )
}

async fn auth_request(
    realm: &str,
    service: &str,
    scope: &str,
) -> Result<AuthResult, Box<dyn std::error::Error>> {
    Ok(
        reqwest::get(format!("{}?service={}&scope={}", realm, service, scope))
            .await?
            .json::<AuthResult>()
            .await?,
    )
}

async fn download(
    url: &str,
    dest: &Path,
    auth: Option<AuthOption>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut request = reqwest::Client::new().get(url);

    match auth {
        None => {}

        Some(AuthOption::Bearer(token)) => request = request.bearer_auth(token),

        Some(AuthOption::Basic(user, password)) => request = request.basic_auth(user, password),
    }

    let res = request.send().await?;

    if res.status().as_u16() < 200 || res.status().as_u16() > 299 {
        panic!(
            "Got a response of status {} when trying to download blob!",
            res.status().as_u16()
        );
    }

    let mut stream = res.bytes_stream();
    let mut file = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(dest)?;

    while let Some(s) = stream.next().await {
        file.write_all(&s?)?;
    }

    file.flush()?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 2 {
        eprintln!("Usage: {} [url]", args[0]);
        std::process::exit(-1);
    }

    let url = args[1].as_str();
    println!("Will download {}...", url);

    // 1 & 2. Check authentication method & authenticate if required
    let authorization_header = match &head_request(url).await? {
        HeadRequestResult::NotFound => {
            eprintln!("Server returned 404 Not found on request!");
            std::process::exit(-2);
        }

        HeadRequestResult::Failed(code) => {
            eprintln!("Request failed with status {} !", code);
            std::process::exit(-3);
        }

        HeadRequestResult::UnauthorizedRealm {
            realm,
            service,
            scope,
        } => {
            println!(
                "Auto-authenticating with realme={}, service={}, scope={}",
                realm, service, scope
            );
            Some(AuthOption::Bearer(
                auth_request(realm, service, scope).await?.token,
            ))
        }

        HeadRequestResult::UnauthorizedBasic => {
            eprintln!("Basic authorization not supported!");
            std::process::exit(-3);
        }

        HeadRequestResult::Ok => None,
    };

    // 3. Download file
    let filename = url.rsplit('/').next().unwrap_or("file");
    let dest = std::env::current_dir()?.join(filename);

    println!("Downloading to {}", dest.as_path().to_str().unwrap());
    download(url, &dest, authorization_header).await?;

    Ok(())
}
